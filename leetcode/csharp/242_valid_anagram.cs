﻿public class _242_valid_anagram
{
    public bool Run(string s, string t)
    {
        if (s.Length != t.Length) return false;
        else
        {
            Dictionary<char, int> firstString = new Dictionary<char, int>();
            Dictionary<char, int> secondString = new Dictionary<char, int>();

            for (int i = 0; i < s.Length; i++)
            {
                if (!firstString.ContainsKey(s[i]))
                    firstString[s[i]] = 1;
                else
                    firstString[s[i]] = firstString[s[i]] + 1;

                if (!secondString.ContainsKey(t[i]))
                    secondString[t[i]] = 1;
                else
                    secondString[t[i]] = secondString[t[i]] + 1;
            }

            foreach (var item in firstString)
            {
                if (!(secondString.ContainsKey(item.Key) && item.Value == secondString[item.Key]))
                    return false;
            }
            return true;
        }
    }
}