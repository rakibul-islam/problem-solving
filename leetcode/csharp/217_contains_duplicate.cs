﻿public class _217_contains_duplicate
{
    public bool Run(int[] nums)
    {
        HashSet<int> numerSet = new HashSet<int>();

        foreach (int num in nums)
        {
            if (numerSet.Contains(num))
                return true;
            numerSet.Add(num);
        }
        return false;
    }
}